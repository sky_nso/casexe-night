<?php

use backend\modules\profile\models\ProfileBalance;
use common\models\User;
use common\modules\prize\models\Prize;
use yii\db\Migration;

/**
 * Class m180627_103742_profile_balance
 */
class m180627_103742_profile_balance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profile_balance%}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'prize_id' => $this->integer()->notNull(),
            'balance' => $this->decimal(65,2)->defaultValue(0),
            'balance_hold' => $this->decimal(65,2)->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_profile_balance_user_id', '{{%profile_balance%}}', 'user_id', User::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_profile_balance_prize_id', '{{%profile_balance%}}', 'prize_id', Prize::tableName(), 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(ProfileBalance::tableName());
    }
}
