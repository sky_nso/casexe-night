<?php

use backend\modules\profile\models\ProfileBalanceTransaction;
use common\models\User;
use common\modules\winner\models\Winner;
use yii\db\Migration;


/**
 * Class m180627_103755_profile_balance_transaction
 */
class m180627_103755_profile_balance_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profile_balance_transaction%}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'winner_id' => $this->integer()->notNull(),
            'value' => $this->decimal(65,2)->defaultValue(0),
            'status' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_profile_balance_transaction_user_id', '{{%profile_balance_transaction%}}', 'user_id', User::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_profile_balance_transaction_winner_id', '{{%profile_balance_transaction%}}', 'winner_id', Winner::tableName(), 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(ProfileBalanceTransaction::tableName());
    }
}
