<?php

use yii\db\Migration;
use common\modules\prize\models\Prize;
/**
 * Class m180627_084241_prize
 */
class m180627_084241_prize extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Prize::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'type' => $this->integer(5)->notNull(),
            'active' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->insert(Prize::tableName(), [
            'name' => Prize::NAME_TYPE_MONEY,
            'type' => Prize::TYPE_MONEY,
            'active' => true,
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert(Prize::tableName(), [
            'name' => Prize::NAME_TYPE_BONUSES,
            'type' => Prize::TYPE_BONUSES,
            'active' => true,
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $itemList = ['horse', 'chair', 'table', 'moose','nut', 'banan', 'bush'];

        foreach ($itemList as $value){
            $this->insert(Prize::tableName(), [
                'name' => $value,
                'type' => Prize::TYPE_ITEM,
                'active' => true,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Prize::tableName());
    }
}
