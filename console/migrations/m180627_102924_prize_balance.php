<?php

use common\modules\prize\models\Prize;
use yii\db\Migration;
use \common\modules\prize\models\PrizeBalance;

/**
 * Class m180627_102924_prize_balance
 */
class m180627_102924_prize_balance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(PrizeBalance::tableName(), [
            'id' => $this->primaryKey(),
            'prize_id' => $this->integer()->notNull(),
            'quantity' => $this->decimal(65, 2)->defaultValue(0),
            'infinity' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_prize_balance_prize_id', PrizeBalance::tableName(), 'prize_id', Prize::tableName(), 'id', 'CASCADE', 'CASCADE');

        $prizeMoneyId = Prize::find()->select(['id'])->where(['type' => Prize::TYPE_MONEY])->one();

        if ($prizeMoneyId) {
            $this->insert(PrizeBalance::tableName(), [
                'prize_id' => $prizeMoneyId->id,
                'quantity' => 10000,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }

        $prizeBonusesId = Prize::find()->select(['id'])->where(['type' => Prize::TYPE_BONUSES])->one();
        if ($prizeBonusesId) {
            $this->insert(PrizeBalance::tableName(), [
                'prize_id' => $prizeBonusesId->id,
                'quantity' => 0,
                'infinity' => true,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }

        $prizeItemIds = Prize::find()->select(['id'])->where(['type' => Prize::TYPE_ITEM])->all();
        if (is_array($prizeItemIds)) {
            foreach ($prizeItemIds as $value) {
                $this->insert(PrizeBalance::tableName(), [
                    'prize_id' => $value->id,
                    'quantity' => 1,
                    'created_at' => time(),
                    'updated_at' => time(),
                ]);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(PrizeBalance::tableName());
    }

}
