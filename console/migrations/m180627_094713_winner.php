<?php

use common\modules\prize\models\Prize;
use yii\db\Migration;
use \common\modules\winner\models\Winner;
use \common\models\User;
/**
 * Class m180627_094713_winner
 */
class m180627_094713_winner extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Winner::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'prize_id' => $this->integer(5)->notNull(),
            'value' => $this->decimal(65,2)->notNull(),
            'status' => $this->string()->defaultValue(false),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_winner_user_id', Winner::tableName(), 'user_id', User::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_winner_prize_id', Winner::tableName(), 'prize_id', Prize::tableName(), 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Winner::tableName());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_094713_winner cannot be reverted.\n";

        return false;
    }
    */
}
