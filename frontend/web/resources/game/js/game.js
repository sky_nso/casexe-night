var game = {
    startGameButton: '#startGame',
    getPrizeButton: '#getPrize',
    rejectPrizeButton: '#rejectPrize',
    urlStartGame: '/game/start-game',
    urlGetPrize: '/game/get-prize',
    urlRejectPrize: '/game/reject-prize',
    winDisplay: '#winDisplay',
    errorDisplay: '#errorDisplay',
    waitingDisplay: '#waitingDisplay',
    startGameBlock: '#game_start',
    questionPrizeBlock: '#game_questions',
    textDisplayBlock: '.prize_information_text',
    textForDisplay: '',
    lastWinnerId: false,

    init: function () {
        game.render(game.questionPrizeBlock, 'hide');
        game.setButtons();
        game.winDisplayView('show');
    },

    startGame: function () {
        $.ajax({
            url: game.urlStartGame,
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (!response.success) {
                    game.viewDisplay(game.errorDisplay)
                    return;
                }

                if (!response.data) {
                    game.setDisplayText('<h1>=(</h1>');
                    game.viewDisplay(game.waitingDisplay);
                    return;
                }

                if (response.data.reject) {
                    game.toggleGameButton();
                    game.lastWinnerId = response.data.id;
                }

                game.setDisplayText('<h2>You Win!</h2><h3>' + response.data.prize.name + '</h3><h4>' + response.data.value + '</h4>');
                game.viewDisplay(game.winDisplay);
            },
            error: function () {
                game.viewDisplay(game.errorDisplay);
            }
        });
    },

    getPrize: function () {
        $.ajax({
            url: game.urlGetPrize,
            type: 'post',
            dataType: 'json',
            data: {
                winnerId: game.lastWinnerId
            },
            success: function (response) {
                if (!response.success) {
                    game.viewDisplay(game.errorDisplay)
                    return;
                }

                game.setDisplayText('<h1>Good!</h1>');
                game.viewDisplay(game.winDisplay);
            },
            error: function (response) {
                game.viewDisplay(game.errorDisplay);
            }
        });

        game.toggleGameButton();
        game.lastWinnerId = false;
    },

    rejectPrize: function () {
        $.ajax({
            url: game.urlRejectPrize,
            type: 'post',
            dataType: 'json',
            data: {
                winnerId: game.lastWinnerId
            },
            success: function (response) {
                if (!response.success) {
                    game.viewDisplay(game.errorDisplay)
                    return;
                }

                game.setDisplayText('<h1>Hmmmm, ok</h1>');
                game.viewDisplay(game.winDisplay);
            },
            error: function (response) {
                game.viewDisplay(game.errorDisplay);
            }
        });

        game.toggleGameButton();
        game.lastWinnerId = false;
    },

    toggleGameButton: function () {
        $(game.startGameBlock).toggle();
        $(game.questionPrizeBlock).toggle();
    },

    viewDisplay: function (display) {
        game.errorDisplayView('hide');
        game.winDisplayView('hide');
        game.waitingDisplayView('hide');

        if (game.getDisplayText()) {
            $(display).find(game.textDisplayBlock).html(game.textForDisplay);
        }

        game.render(display, 'show');
        game.resetDisplayText();
    },

    errorDisplayView: function (state) {
        game.render(game.errorDisplay, state);
    },

    winDisplayView: function (state) {
        game.render(game.winDisplay, state);
    },

    waitingDisplayView: function (state) {
        game.render(game.waitingDisplay, state);
    },

    render: function (display, state) {

        switch (state) {
            case 'show':
                $(display).show();
                break;
            case 'hide':
                $(display).hide();
                break;
        }
    },

    setDisplayText: function (text) {
        game.textForDisplay = text;
    },

    getDisplayText: function () {
        if (game.textForDisplay && typeof game.textForDisplay === 'string' && game.textForDisplay != '') {
            return game.textForDisplay;
        }

        return false;
    },

    resetDisplayText: function () {
        game.textForDisplay = '';
    },

    setButtons: function () {
        $(game.startGameButton).on('click', function () {
            game.startGame();
        });

        $(game.getPrizeButton).on('click', function () {
            game.getPrize();
        });

        $(game.rejectPrizeButton).on('click', function () {
            game.rejectPrize();
        });

    },

}

game.init();

