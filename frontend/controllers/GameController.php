<?php
namespace frontend\controllers;

use backend\modules\profile\models\ProfileBalanceTransaction;
use common\components\base\AjaxFilter;
use common\modules\prize\models\PrizeBalance;
use common\modules\winner\models\WinnerStatus;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\modules\prize\models\Prize;
use common\components\Winner;
use common\modules\winner\models\Winner as WinnerCommon;

/**
 * Game controller
 */
class GameController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'start-game',
                    'get-prize',
                    'reject-prize'
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function actionStartGame()
    {
        $types = Prize::getCollectionTypeID();

        shuffle($types);
        $type = rand(0, count(Prize::getCollectionTypeID()) - 1);

        switch ($type) {
            case Prize::TYPE_MONEY:
                $data = Winner::getPrizeMoney();
                break;
            case Prize::TYPE_ITEM:
                $data = Winner::getPrizeItem();
                break;
            case Prize::TYPE_BONUSES:
            default:
                $data = Winner::getPrizeBonuses();
                break;
        }

        return [
            'success' => true,
            'data' => $data
        ];
    }

    /**
     * @return array
     */
    public function actionGetPrize()
    {
        if (!Yii::$app->request->post('winnerId')) {
            return [
                'success' => false,
            ];
        }

        if(!Winner::setStatusWinner(WinnerStatus::STATUS_ACCEPTED, ['id' => Yii::$app->request->post('winnerId'), 'user_id' => Yii::$app->user->id])){
            return [
                'success' => false,
            ];
        }

        return [
            'success' => true,
        ];
    }

    public function actionRejectPrize()
    {
        if (!Yii::$app->request->post('winnerId')) {
            return [
                'success' => false,
            ];
        }

        if(!Winner::setStatusWinner(WinnerStatus::STATUS_REJECTED, ['id' => Yii::$app->request->post('winnerId'), 'user_id' => Yii::$app->user->id])){
            return [
                'success' => false,
            ];
        }

        $winnerModel = WinnerCommon::findOne(['id' => Yii::$app->request->post('winnerId')]);
        $balance = PrizeBalance::findOne(['prize_id' => $winnerModel->prize_id]);

        Winner::incrementBalance($balance, $winnerModel->value);

        return [
            'success' => true,
        ];
    }

}