<?php
use yii\helpers\Html;

$this->title = 'Welcome to '.Yii::$app->name;
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            Please <?= Html::a('LogIn', '/site/login');?> or <?= Html::a('SingUp', '/site/signup');?>
        </div>

    </div>
</div>
