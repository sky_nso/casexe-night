<?php
use frontend\assets\GameAsset;

$this->title = Yii::$app->name;
GameAsset::register($this);
?>
<div class="site-index">

    <div class="jumbotron">
        <div class="panel panel-default prize_information_block">
            <div id="waitingDisplay" class="display_prize bg-warning"><div class="prize_information_text"></div></div>
            <div id="winDisplay" class="display_prize bg-success"><div class="prize_information_text">
                    <h1>Let`s Go!</h1>
                </div>
            </div>
            <div id="errorDisplay" class="display_prize bg-danger">
                <div class="prize_information_text">
                    <h2>Houston, we have a problem!</h2>
                    <h3>Sorry, try again later =( </h3>
                </div>
            </div>
        </div>
        <div class="row" id="game_start">
            <div class="btn btn-lg btn-primary" id="startGame">Go!</div>
        </div>
        <div class="row" id="game_questions">
            <div class="btn btn-lg btn-success" id="getPrize">Get prize now</div>
            <div class="btn btn-lg btn-danger" id="rejectPrize">Reject prize</div>
        </div>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Items</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
            </div>

            <div class="col-lg-4">
                <h2>Money</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
            </div>

            <div class="col-lg-4">
                <h2>Bonuses of loyalty</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
            </div>
        </div>

    </div>
</div>
