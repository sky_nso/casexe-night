<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class GameAsset
 * @package frontend\assets
 */
class GameAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/game/';
    public $css = [
        'css/game.css',
    ];
    public $js = [
        'js/game.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
