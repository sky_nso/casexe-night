<?php
namespace backend\modules\profile;

use yii\base\Module as YiiModule;

/**
 * Class Module
 * @package common\modules\profile
 */
class Module extends YiiModule
{
    public $controllerNamespace = 'backend\backend\profile\controllers';
}
