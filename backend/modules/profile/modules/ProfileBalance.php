<?php
namespace backend\modules\profile\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class ProfileBalance
 * @property integer id
 * @property integer user_id
 * @property integer prize_id
 * @property float balance
 * @property float balance_hold
 * @property integer created_at
 * @property integer updated_at
 */
class ProfileBalance extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%profile_balance%}}';
    }
}