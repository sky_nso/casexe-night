<?php
namespace backend\modules\profile\models;

use Yii;
use yii\db\ActiveRecord;

/**
* Class ProfileBalanceTransaction
* @property integer id
* @property integer user_id
* @property integer winner_id
* @property float value
* @property string status
* @property integer created_at
* @property integer updated_at
*/
class ProfileBalanceTransaction extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%profile_balance_transaction%}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }
}