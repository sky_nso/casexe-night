<?php
namespace backend\modules\profile\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class ProfileBalanceTransactionStatus
 */
class ProfileBalanceTransactionStatus extends ActiveRecord
{
    const STATUS_PENDING = 'pending';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_DONE = 'done';
    const STATUS_FUNDS = 'funds';
    const STATUS_REFUNDS = 'refunds';
    const STATUS_ERROR = 'error';
}