<?php
namespace common\modules\prize\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class PrizeBalance
 * @property integer id
 * @property integer prize_id
 * @property integer quantity
 * @property boolean infinity
 * @property integer created_at
 * @property integer updated_at
 */
class PrizeBalance extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%prize_balance%}}';
    }

    /**
     * @param int $id
     * @return null|static
     */
    public static function getBalanceByPrizeId(int $id)
    {
        return self::findOne(['prize_id' => $id]);
    }

    /**
     * @param self $balance
     * @param float $value
     * @return bool
     */
    public static function canDecrementBalance(self $balance, float $value)
    {
        if ($balance->infinity || $balance->quantity < $value) {
            return false;
        }

        return true;
    }

    public static function decrementBalance(self $balance, float $value)
    {
        if (!self::canDecrementBalance($balance, $value)) {
            return false;
        }

        $balance->quantity -= $value;
        $balance->save();

        return true;
    }

    public static function incrementBalance(self $balance, float $value)
    {
        if ($balance->infinity) {
            return true;
        }

        $balance->quantity += $value;
        $balance->save();

        return true;
    }

    public function getPrize()
    {
        return $this->hasOne(Prize::className(), ['id' => 'prize_id']);
    }


}