<?php
namespace common\modules\prize\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class Prize
 * @package common\modules\prize\models
 * @property int id
 * @property string name
 * @property int type
 * @property boolean active
 * @property int created_at
 * @property int updated_at
 * @property PrizeBalance object balance
 */
class Prize extends ActiveRecord
{
    const TYPE_MONEY = 1;
    const TYPE_ITEM = 2;
    const TYPE_BONUSES = 3;

    const NAME_TYPE_MONEY = 'Money';
    const NAME_TYPE_ITEM = 'Item';
    const NAME_TYPE_BONUSES = 'Bonuses of loyalty';

    const MIN_MONEY_PRIZE = 500;
    const MAX_MONEY_PRIZE = 1000;

    const MIN_QUANTITY_PRIZE_ITEM = 1;

    /**
     * @return array
     */
    public static function getMapping()
    {
        return [
            self::TYPE_MONEY => self::NAME_TYPE_MONEY,
            self::TYPE_ITEM => self::NAME_TYPE_ITEM,
            self::TYPE_BONUSES => self::NAME_TYPE_BONUSES,
        ];

    }

    /**
     * @return array
     */
    public static function getCollectionTypeID()
    {
        return [
            self::TYPE_MONEY,
            self::TYPE_ITEM,
            self::TYPE_BONUSES
        ];
    }

    /**
     * @return array
     */
    public static function getCollectionName()
    {
        return [
            self::NAME_TYPE_MONEY,
            self::NAME_TYPE_ITEM,
            self::NAME_TYPE_BONUSES
        ];
    }

    /**
     * @param int $id
     * @return bool|mixed
     */
    public static function getTypeName(int $id)
    {
        $mapping = self::getMapping();

        if(!isset($mapping[$id])){
            return false;
        }

        return $mapping[$id];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%prize%}}';
    }

    public function getBalance()
    {
        return $this->hasMany(PrizeBalance::className(), ['prize_id' => 'id']);
    }

}