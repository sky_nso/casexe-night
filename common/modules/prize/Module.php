<?php
namespace common\modules\prize;

use yii\base\Module as YiiModule;

/**
 * Class Module
 * @package common\modules\prize
 */
class Module extends YiiModule
{
    public $controllerNamespace = 'common\modules\prize\controllers';
}
