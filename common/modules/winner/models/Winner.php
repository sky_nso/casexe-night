<?php
namespace common\modules\winner\models;

use common\modules\prize\models\Prize;
use Yii;
use yii\db\ActiveRecord;

/**
 * Prize model
 * @property integer id
 * @property integer user_id
 * @property integer prize_id
 * @property float value
 * @property string status
 * @property integer created_at
 * @property integer updated_at
 */
class Winner extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%winner%}}';
    }

    /**
     * @return array
     * todo а вообще правильнее это вынести в свой ActiveRecord
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function getPrize()
    {
        return $this->hasOne(Prize::className(), ['id' => 'prize_id']);
    }
}