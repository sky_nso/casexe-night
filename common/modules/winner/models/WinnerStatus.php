<?php
namespace common\modules\winner\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * WinnerStatus model
 *
 */
class WinnerStatus extends ActiveRecord
{

    const STATUS_WAITING = 'waiting';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_REJECTED = 'rejected';
    const STATUS_DONE = 'done';

    /**
     * @return array
     */
    public static function getCollection()
    {
        return [
            self::STATUS_WAITING,
            self::STATUS_ACCEPTED,
            self::STATUS_REJECTED,
            self::STATUS_DONE,
        ];
    }

}
