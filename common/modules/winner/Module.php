<?php
namespace common\modules\winner;

use yii\base\Module as YiiModule;

/**
 * Class Module
 * @package common\modules\winner
 */
class Module extends YiiModule
{
    public $controllerNamespace = 'common\modules\winner\controllers';
}
