<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'prize' => [
            'class' => 'common\modules\prize\Module',
        ],
        'winner' => [
            'class' => 'common\modules\winner\Module',
        ],
    ]
];