<?php

namespace common\components;

use common\modules\prize\models\Prize;
use common\modules\prize\models\PrizeBalance;
use common\modules\winner\models\WinnerStatus;
use Yii;
use yii\base\Component;
use common\modules\winner\models\winner as WinnerCommon;


/**
 * Class Winner
 * @package common\components
 */
class Winner extends Component
{
    /**
     * @return bool|int
     */
    public static function getPrizeMoney()
    {
        $balance = PrizeBalance::find()
            ->leftJoin(Prize::tableName(), PrizeBalance::tableName().'.prize_id = '. Prize::tableName().'.id')
            ->where([Prize::tableName().'.type' => Prize::TYPE_MONEY])
            ->one();

        $value = rand(Prize::MIN_MONEY_PRIZE, Prize::MAX_MONEY_PRIZE);

        if (!PrizeBalance::decrementBalance($balance, $value)) {
            $value = $balance->quantity;
            if (!PrizeBalance::decrementBalance($balance, $value) || $value <= 0) {
                return false;
            }
        }

        $winnerTransaction = self::addWinner(Yii::$app->user->id, $balance->prize->id, $value, WinnerStatus::STATUS_WAITING);

        if (!$winnerTransaction) {
            return false;
        }

        return self::returnResult($winnerTransaction);
    }

    /**
     * @return array|bool
     */
    public static function getPrizeItem()
    {
        $balances = PrizeBalance::find()
            ->leftJoin(Prize::tableName(), PrizeBalance::tableName().'.prize_id = '. Prize::tableName().'.id')
            ->where([Prize::tableName().'.type' => Prize::TYPE_ITEM])
            ->andWhere(['or',['infinity' => true], ['>=', 'quantity', Prize::MIN_QUANTITY_PRIZE_ITEM]])
            ->all();

        if(empty($balances)){
            return false;
        }

        $balance = $balances[rand(0,count($balances) - 1)];

        if (!self::decrementBalance($balance, Prize::MIN_QUANTITY_PRIZE_ITEM)) {
            return false;
        }

        $winnerTransaction = self::addWinner(Yii::$app->user->id, $balance->prize->id, Prize::MIN_QUANTITY_PRIZE_ITEM, WinnerStatus::STATUS_WAITING);

        if (!$winnerTransaction) {
            return false;
        }

        return self::returnResult($winnerTransaction);

    }

    /**
     * @return array|bool
     */
    public static function getPrizeBonuses()
    {
        $balance = PrizeBalance::find()
            ->leftJoin(Prize::tableName(), PrizeBalance::tableName().'.prize_id = '. Prize::tableName().'.id')
            ->where([Prize::tableName().'.type' => Prize::TYPE_BONUSES])
            ->one();

        /***
         * some code to determine the number of bonuses
         */
        $value = 100;

        self::decrementBalance($balance, $value);

        $winnerTransaction = self::addWinner(Yii::$app->user->id, $balance->prize->id, $value, WinnerStatus::STATUS_WAITING);

        if (!$winnerTransaction) {
            return false;
        }

        return self::returnResult($winnerTransaction);
    }

    /**
     * @param int $userId
     * @param int $prize_id
     * @param float $value
     * @param string $status
     * @return bool|WinnerCommon
     */
    public static function addWinner(int $userId, int $prize_id, float $value, string $status)
    {
        $winner = new WinnerCommon();

        $winner->user_id = $userId;
        $winner->prize_id = $prize_id;
        $winner->value = $value;
        $winner->status = $status;

        if (!$winner->save()) {
            return false;
        }

        return $winner;
    }

    /**
     * @param string $status
     * @param array $params
     * @return bool
     */
    public static function setStatusWinner(string $status, array $params)
    {
        $winner = WinnerCommon::findOne($params);
        $winner->status = $status;

        if(!$winner->save()){
            return false;
        }

        return true;
    }

    /**
     * @param WinnerCommon $winnerCommon
     * @param bool $reject
     * @return array
     */
    public function returnResult($winnerCommon, $reject = true)
    {
        return [
            'prize' => $winnerCommon->prize,
            'value' => $winnerCommon->value,
            'reject' => $reject,
            'id' => $winnerCommon->id
        ];
    }

    /**
     * @param PrizeBalance $balance
     * @param float $value
     * @return bool
     */
    public static function incrementBalance(PrizeBalance $balance, float $value)
    {
        return PrizeBalance::incrementBalance($balance, $value);
    }

    /**
     * @param PrizeBalance $balance
     * @param float $value
     * @return bool
     */
    public static function decrementBalance(PrizeBalance $balance, float $value)
    {
        return PrizeBalance::decrementBalance($balance, $value);
    }
}